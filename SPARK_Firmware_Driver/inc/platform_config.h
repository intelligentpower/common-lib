/**
 ******************************************************************************
 * @file    platform_config.h
 * @author  Satish Nair, Zachary Crockett, Mohit Bhoite, and Logan Anteau
 * @version V1.0.1
 * @date    03-December-2014
 * @brief   Board specific configuration file.
 ******************************************************************************
    Copyright (c) 2013 Spark Labs, Inc.  All rights reserved.
    Copyright (c) 2014 Logan Anteau. All rights reserved.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation, either
    version 3 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, see <http://www.gnu.org/licenses/>.
    ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PLATFORM_CONFIG_H
#define __PLATFORM_CONFIG_H

/* Uncomment the line below to enable WLAN, WIRING, SFLASH and RTC functionality */
#define SPARK_WLAN_ENABLE
#define SPARK_WIRING_ENABLE
#define SPARK_SFLASH_ENABLE
#define SPARK_RTC_ENABLE

#define         ID1          (0x1FFF7A10)
#define         ID2          (0x1FFF7A14)
#define         ID3          (0x1FFF7A18)

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

/* Exported macro ------------------------------------------------------------*/

//LEDs
#define LEDn                              3
#define LED2_GPIO_PIN                     GPIO_Pin_0 /* RGB: Blue LED */
#define LED2_GPIO_PORT                    GPIOA
#define LED2_GPIO_CLK                     RCC_AHB1Periph_GPIOA
#define LED3_GPIO_PIN                     GPIO_Pin_1 /* RGB: Red LED */
#define LED3_GPIO_PORT                    GPIOA
#define LED3_GPIO_CLK                     RCC_AHB1Periph_GPIOA
#define LED4_GPIO_PIN                     GPIO_Pin_2
#define LED4_GPIO_PORT                    GPIOA
#define LED4_GPIO_CLK                     RCC_AHB1Periph_GPIOA

//Push Buttons
#define BUTTONn                           1
#define BUTTON1_GPIO_PIN                  GPIO_Pin_6
#define BUTTON1_GPIO_PORT                 GPIOA
#define BUTTON1_GPIO_CLK                  RCC_AHB1Periph_GPIOA
#define BUTTON1_GPIO_MODE                 GPIO_Mode_IN
#define BUTTON1_GPIO_PUPD                 GPIO_PuPd_UP
#define BUTTON1_PRESSED                   0x00
#define BUTTON1_EXTI_LINE                 EXTI_Line6
#define BUTTON1_EXTI_PORT_SOURCE          EXTI_PortSourceGPIOA
#define BUTTON1_EXTI_PIN_SOURCE           GPIO_PinSource6
#define BUTTON1_EXTI_IRQn                 EXTI9_5_IRQn
#define BUTTON1_EXTI_TRIGGER              EXTI_Trigger_Falling

//Power Strip GPIO
#define OUTLET_0_PIN                      GPIO_Pin_7
#define OUTLET_0_PORT                     GPIOD
#define OUTLET_0_CLK                      RCC_AHB1Periph_GPIOD
#define OUTLET_1_PIN                      GPIO_Pin_6
#define OUTLET_1_PORT                     GPIOD
#define OUTLET_1_CLK                      RCC_AHB1Periph_GPIOD
#define OUTLET_2_PIN                      GPIO_Pin_5
#define OUTLET_2_PORT                     GPIOD
#define OUTLET_2_CLK                      RCC_AHB1Periph_GPIOD
#define OUTLET_3_PIN                      GPIO_Pin_4
#define OUTLET_3_PORT                     GPIOD
#define OUTLET_3_CLK                      RCC_AHB1Periph_GPIOD
#define OUTLET_4_PIN                      GPIO_Pin_3
#define OUTLET_4_PORT                     GPIOD
#define OUTLET_4_CLK                      RCC_AHB1Periph_GPIOD
#define OUTLET_5_PIN                      GPIO_Pin_2
#define OUTLET_5_PORT                     GPIOD
#define OUTLET_5_CLK                      RCC_AHB1Periph_GPIOD

//Power Strip ADC GPIO
#define ADC_CONVST_PIN                    GPIO_Pin_8
#define ADC_CONVST_PORT                   GPIOE
#define ADC_CONVST_CLK                    RCC_AHB1Periph_GPIOE
#define ADC_EOC_PIN                       GPIO_Pin_9
#define ADC_EOC_PORT                      GPIOE
#define ADC_EOC_CLK                       RCC_AHB1Periph_GPIOE
#define ADC_DB0_PIN                       GPIO_Pin_10
#define ADC_DB0_PORT                      GPIOE
#define ADC_DB0_CLK                       RCC_AHB1Periph_GPIOE
#define ADC_DB1_PIN                       GPIO_Pin_11
#define ADC_DB1_PORT                      GPIOE
#define ADC_DB1_CLK                       RCC_AHB1Periph_GPIOE
#define ADC_DB2_PIN                       GPIO_Pin_12
#define ADC_DB2_PORT                      GPIOE
#define ADC_DB2_CLK                       RCC_AHB1Periph_GPIOE
#define ADC_DB3_PIN                       GPIO_Pin_13
#define ADC_DB3_PORT                      GPIOE
#define ADC_DB3_CLK                       RCC_AHB1Periph_GPIOE
#define ADC_DB4_PIN                       GPIO_Pin_8
#define ADC_DB4_PORT                      GPIOD
#define ADC_DB4_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB5_PIN                       GPIO_Pin_9
#define ADC_DB5_PORT                      GPIOD
#define ADC_DB5_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB6_PIN                       GPIO_Pin_10
#define ADC_DB6_PORT                      GPIOD
#define ADC_DB6_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB7_PIN                       GPIO_Pin_11
#define ADC_DB7_PORT                      GPIOD
#define ADC_DB7_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB8_PIN                       GPIO_Pin_12
#define ADC_DB8_PORT                      GPIOD
#define ADC_DB8_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB9_PIN                       GPIO_Pin_13
#define ADC_DB9_PORT                      GPIOD
#define ADC_DB9_CLK                       RCC_AHB1Periph_GPIOD
#define ADC_DB10_PIN                      GPIO_Pin_14
#define ADC_DB10_PORT                     GPIOD
#define ADC_DB10_CLK                      RCC_AHB1Periph_GPIOD
#define ADC_DB11_PIN                      GPIO_Pin_15
#define ADC_DB11_PORT                     GPIOD
#define ADC_DB11_CLK                      RCC_AHB1Periph_GPIOD
#define ADC_DB12_PIN                      GPIO_Pin_6
#define ADC_DB12_PORT                     GPIOC
#define ADC_DB12_CLK                      RCC_AHB1Periph_GPIOC
#define ADC_DB13_PIN                      GPIO_Pin_7
#define ADC_DB13_PORT                     GPIOC
#define ADC_DB13_CLK                      RCC_AHB1Periph_GPIOC
#define ADC_DB14_PIN                      GPIO_Pin_8
#define ADC_DB14_PORT                     GPIOC
#define ADC_DB14_CLK                      RCC_AHB1Periph_GPIOC
#define ADC_DB15_PIN                      GPIO_Pin_9
#define ADC_DB15_PORT                     GPIOC
#define ADC_DB15_CLK                      RCC_AHB1Periph_GPIOC
#define ADC_WR_PIN                        GPIO_Pin_8
#define ADC_WR_PORT                       GPIOA
#define ADC_WR_CLK                        RCC_AHB1Periph_GPIOA
#define ADC_RD_PIN                        GPIO_Pin_9
#define ADC_RD_PORT                       GPIOA
#define ADC_RD_CLK                        RCC_AHB1Periph_GPIOA
#define ADC_CS_PIN                        GPIO_Pin_10
#define ADC_CS_PORT                       GPIOA
#define ADC_CS_CLK                        RCC_AHB1Periph_GPIOA
#define ADC_EOC_EXTI_LINE                 EXTI_Line9
#define ADC_EOC_EXTI_PORT_SOURCE          EXTI_PortSourceGPIOE
#define ADC_EOC_EXTI_PIN_SOURCE           GPIO_PinSource9
#define ADC_EOC_EXTI_IRQn                 EXTI9_5_IRQn
#define ADC_EOC_EXTI_TRIGGER              EXTI_Trigger_Falling

//CC3000 Interface pins
#define CC3000_SPI                        SPI2
#define CC3000_SPI_CLK                    RCC_APB1Periph_SPI2
#define CC3000_SPI_CLK_CMD                RCC_APB1PeriphClockCmd
#define CC3000_SPI_SCK_GPIO_PIN           GPIO_Pin_13
#define CC3000_SPI_SCK_GPIO_PIN_SOURCE    GPIO_PinSource13
#define CC3000_SPI_SCK_GPIO_PORT          GPIOB
#define CC3000_SPI_SCK_GPIO_CLK           RCC_AHB1Periph_GPIOB
#define CC3000_SPI_MISO_GPIO_PIN          GPIO_Pin_14
#define CC3000_SPI_MISO_GPIO_PIN_SOURCE   GPIO_PinSource14
#define CC3000_SPI_MISO_GPIO_PORT         GPIOB
#define CC3000_SPI_MISO_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define CC3000_SPI_MOSI_GPIO_PIN          GPIO_Pin_15
#define CC3000_SPI_MOSI_GPIO_PIN_SOURCE   GPIO_PinSource15
#define CC3000_SPI_MOSI_GPIO_PORT         GPIOB
#define CC3000_SPI_MOSI_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define CC3000_WIFI_CS_GPIO_PIN           GPIO_Pin_10
#define CC3000_WIFI_CS_GPIO_PORT          GPIOB
#define CC3000_WIFI_CS_GPIO_CLK           RCC_AHB1Periph_GPIOB
#define CC3000_WIFI_EN_GPIO_PIN           GPIO_Pin_15
#define CC3000_WIFI_EN_GPIO_PORT          GPIOE
#define CC3000_WIFI_EN_GPIO_CLK           RCC_AHB1Periph_GPIOE
#define CC3000_WIFI_INT_GPIO_PIN          GPIO_Pin_14
#define CC3000_WIFI_INT_GPIO_PORT         GPIOE
#define CC3000_WIFI_INT_GPIO_CLK          RCC_AHB1Periph_GPIOE

#define CC3000_WIFI_INT_EXTI_LINE         EXTI_Line14
#define CC3000_WIFI_INT_EXTI_PORT_SOURCE  EXTI_PortSourceGPIOE
#define CC3000_WIFI_INT_EXTI_PIN_SOURCE   EXTI_PinSource14
#define CC3000_WIFI_INT_EXTI_IRQn         EXTI15_10_IRQn

#define CC3000_SPI_DMA_CLK                RCC_AHB1Periph_DMA1
#define CC3000_SPI_RX_DMA_CHANNEL         DMA_Channel_0
#define CC3000_SPI_RX_DMA_STREAM          DMA1_Stream3
#define CC3000_SPI_TX_DMA_CHANNEL         DMA_Channel_0
#define CC3000_SPI_TX_DMA_STREAM          DMA1_Stream4
#define CC3000_SPI_RX_DMA_TCFLAG          DMA_FLAG_TCIF3
#define CC3000_SPI_TX_DMA_TCFLAG          DMA_FLAG_TCIF4
#define CC3000_SPI_RX_DMA_IRQn            DMA1_Stream3_IRQn
#define CC3000_SPI_TX_DMA_IRQn            DMA1_Stream4_IRQn

#define CC3000_SPI_DR_BASE                ((uint32_t)(&(CC3000_SPI->DR)))
#define CC3000_SPI_BAUDRATE_PRESCALER     SPI_BaudRatePrescaler_8

//SST25 FLASH Interface pins
#define sFLASH_SPI                        SPI2
#define sFLASH_SPI_CLK                    RCC_APB1Periph_SPI2
#define sFLASH_SPI_CLK_CMD                RCC_APB1PeriphClockCmd
#define sFLASH_SPI_SCK_GPIO_PIN           GPIO_Pin_13                    /* PB.13 */
#define sFLASH_SPI_SCK_GPIO_PIN_SOURCE    GPIO_PinSource13
#define sFLASH_SPI_SCK_GPIO_PORT          GPIOB                        /* GPIOB */
#define sFLASH_SPI_SCK_GPIO_CLK           RCC_AHB1Periph_GPIOB
#define sFLASH_SPI_MISO_GPIO_PIN          GPIO_Pin_14                    /* PB.14 */
#define sFLASH_SPI_MISO_GPIO_PIN_SOURCE   GPIO_PinSource14
#define sFLASH_SPI_MISO_GPIO_PORT         GPIOB                        /* GPIOB */
#define sFLASH_SPI_MISO_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define sFLASH_SPI_MOSI_GPIO_PIN          GPIO_Pin_15                    /* PB.15 */
#define sFLASH_SPI_MOSI_GPIO_PIN_SOURCE   GPIO_PinSource15
#define sFLASH_SPI_MOSI_GPIO_PORT         GPIOB                        /* GPIOB */
#define sFLASH_SPI_MOSI_GPIO_CLK          RCC_AHB1Periph_GPIOB
#define sFLASH_MEM_CS_GPIO_PIN            GPIO_Pin_11                    /* PB.09 */
#define sFLASH_MEM_CS_GPIO_PORT           GPIOB                        /* GPIOB */
#define sFLASH_MEM_CS_GPIO_CLK            RCC_AHB1Periph_GPIOB

#define sFLASH_SPI_BAUDRATE_PRESCALER     SPI_BaudRatePrescaler_8

#define USB_DISCONNECT_GPIO_PIN           GPIO_Pin_7
#define USB_DISCONNECT_GPIO_PORT          GPIOA
#define USB_DISCONNECT_GPIO_CLK           RCC_AHB1Periph_GPIOA

#define UI_TIMER_FREQUENCY                100                            /* 100Hz -> 10ms */
#define BUTTON_DEBOUNCE_INTERVAL          1000 / UI_TIMER_FREQUENCY

//NVIC Priorities based on NVIC_PriorityGroup_4
#define DMA1_STREAM4_IRQ_PRIORITY         0    //CC3000_SPI_TX_DMA Interrupt
#define EXTI15_10_IRQ_PRIORITY            1    //CC3000_WIFI_INT_EXTI & User Interrupt
#define ADC_EOC_IRQ_PRIORITY              2    //USB_LP_CAN1_RX0 Interrupt
#define TIM3_CC_IRQ_PRIORITY              3    //ADC Timing Interrupt
#define RTC_IRQ_PRIORITY                  4    //RTC Seconds Interrupt
#define TIM5_CC_IRQ_PRIORITY              5    //TIM5_CC4 Interrupt
#define EXTI9_5_IRQ_PRIORITY              6    //BUTTON1 EXTI Interrupt
#define USART2_IRQ_PRIORITY               7    //USART2 Interrupt
#define EXTI0_IRQ_PRIORITY                11    //User Interrupt
#define EXTI1_IRQ_PRIORITY                11    //User Interrupt
#define EXTI3_IRQ_PRIORITY                11    //User Interrupt
#define EXTI4_IRQ_PRIORITY                11    //User Interrupt
#define SYSTICK_IRQ_PRIORITY              13    //CORTEX_M3 Systick Interrupt
#define SVCALL_IRQ_PRIORITY               14    //CORTEX_M3 SVCall Interrupt
#define PENDSV_IRQ_PRIORITY               15    //CORTEX_M3 PendSV Interrupt

/* Exported functions ------------------------------------------------------- */

#endif /* __PLATFORM_CONFIG_H */
