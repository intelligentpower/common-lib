/**
 ******************************************************************************
 * @file    hw_config.c
 * @author  Satish Nair, Zachary Crockett, Mohit Bhoite and Logan Anteau
 * @version V1.0.1
 * @date    03-December-2014
 * @brief   Hardware Configuration & Setup
 ******************************************************************************
  Copyright (c) 2013 Spark Labs, Inc.  All rights reserved.
  Copyright (c) 2014 Logan Anteau. All rights reserved.

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation, either
  version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this program; if not, see <http://www.gnu.org/licenses/>.
  ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"
#include <string.h>
#include "spi_bus.h"
#include "debug.h"
#include "cc3000_spi.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint8_t USE_SYSTEM_FLAGS = 0;   //0, 1
uint16_t sys_health_cache = 0; // Used by the SYS_HEALTH macros store new heath if higher

volatile uint32_t TimingDelay;
volatile uint32_t TimingLED;
volatile uint32_t TimingBUTTON;
volatile uint32_t TimingIWDGReload;
volatile uint32_t TimingNTP;
volatile uint32_t TimingOutletTimers;
volatile uint32_t TimingOutletStatus;

__IO uint8_t IWDG_SYSTEM_RESET;

GPIO_TypeDef* OUTLET_GPIO_PORT[] = {OUTLET_0_PORT, OUTLET_1_PORT, OUTLET_2_PORT,
                                OUTLET_3_PORT, OUTLET_4_PORT, OUTLET_5_PORT};
const uint16_t OUTLET_GPIO_PIN[] = {OUTLET_0_PIN, OUTLET_1_PIN, OUTLET_2_PIN,
                                OUTLET_3_PIN, OUTLET_4_PIN, OUTLET_5_PIN};
const uint32_t OUTLET_GPIO_CLK[] = {OUTLET_0_CLK, OUTLET_1_CLK, OUTLET_2_CLK,
                                OUTLET_3_CLK, OUTLET_4_CLK, OUTLET_5_CLK};

GPIO_TypeDef* ADC_CR_PORT[] = {ADC_DB0_PORT, ADC_DB1_PORT, ADC_DB2_PORT,
                            ADC_DB3_PORT};
const uint16_t ADC_CR_PIN[] = {ADC_DB0_PIN, ADC_DB1_PIN, ADC_DB2_PIN, 
                            ADC_DB3_PIN};
const uint32_t ADC_CR_CLK[] = {ADC_DB0_CLK, ADC_DB1_CLK, ADC_DB2_CLK,
                            ADC_DB3_CLK};
GPIO_TypeDef* ADC_DI_PORT[] = {ADC_DB0_PORT, ADC_DB1_PORT, ADC_DB2_PORT,
                            ADC_DB3_PORT, ADC_DB4_PORT, ADC_DB5_PORT,
                            ADC_DB6_PORT, ADC_DB7_PORT, ADC_DB8_PORT,
                            ADC_DB9_PORT, ADC_DB10_PORT, ADC_DB11_PORT,
                            ADC_DB12_PORT, ADC_DB13_PORT, ADC_DB14_PORT,
                            ADC_DB15_PORT};
const uint16_t ADC_DI_PIN[] = {ADC_DB0_PIN, ADC_DB1_PIN, ADC_DB2_PIN,
                            ADC_DB3_PIN, ADC_DB4_PIN, ADC_DB5_PIN,
                            ADC_DB6_PIN, ADC_DB7_PIN, ADC_DB8_PIN,
                            ADC_DB9_PIN, ADC_DB10_PIN, ADC_DB11_PIN,
                            ADC_DB12_PIN, ADC_DB13_PIN, ADC_DB14_PIN,
                            ADC_DB15_PIN};
const uint32_t ADC_DI_CLK[] = {ADC_DB0_CLK, ADC_DB1_CLK, ADC_DB2_CLK,
                            ADC_DB3_CLK, ADC_DB4_CLK, ADC_DB5_CLK,
                            ADC_DB6_CLK, ADC_DB7_CLK, ADC_DB8_CLK,
                            ADC_DB9_CLK, ADC_DB10_CLK, ADC_DB11_CLK,
                            ADC_DB12_CLK, ADC_DB13_CLK, ADC_DB14_CLK,
                            ADC_DB15_CLK};

GPIO_TypeDef* LED_GPIO_PORT[] = {LED2_GPIO_PORT, LED3_GPIO_PORT, LED4_GPIO_PORT};
const uint16_t LED_GPIO_PIN[] = {LED2_GPIO_PIN, LED3_GPIO_PIN, LED4_GPIO_PIN};
const uint32_t LED_GPIO_CLK[] = {LED2_GPIO_CLK, LED3_GPIO_CLK, LED4_GPIO_CLK};
__IO uint16_t LED_TIM_CCR[] = {0x0000, 0x0000, 0x0000, 0x0000};
__IO uint16_t LED_TIM_CCR_SIGNAL[] = {0x0000, 0x0000, 0x0000, 0x0000};  //TIM CCR Signal Override
uint8_t LED_RGB_OVERRIDE = 0;
uint8_t LED_RGB_BRIGHTNESS = 96;
uint32_t lastSignalColor = 0;
uint32_t lastRGBColor = 0;

/* Led Fading. */
#define NUM_LED_FADE_STEPS 100 /* Called at 100Hz, fade over 1 second. */
static uint8_t led_fade_step = NUM_LED_FADE_STEPS - 1;
static int8_t led_fade_direction = -1; /* 1 = rising, -1 = falling. */

GPIO_TypeDef* BUTTON_GPIO_PORT[] = {BUTTON1_GPIO_PORT};
const uint16_t BUTTON_GPIO_PIN[] = {BUTTON1_GPIO_PIN};
const uint32_t BUTTON_GPIO_CLK[] = {BUTTON1_GPIO_CLK};
GPIOMode_TypeDef BUTTON_GPIO_MODE[] = {BUTTON1_GPIO_MODE};
GPIOPuPd_TypeDef BUTTON_GPIO_PUPD[] = {BUTTON1_GPIO_PUPD};
__IO uint16_t BUTTON_DEBOUNCED_TIME[] = {0, 0};

const uint16_t BUTTON_EXTI_LINE[] = {BUTTON1_EXTI_LINE};
const uint16_t BUTTON_GPIO_PORT_SOURCE[] = {BUTTON1_EXTI_PORT_SOURCE};
const uint16_t BUTTON_GPIO_PIN_SOURCE[] = {BUTTON1_EXTI_PIN_SOURCE};
const uint16_t BUTTON_IRQn[] = {BUTTON1_EXTI_IRQn};
EXTITrigger_TypeDef BUTTON_EXTI_TRIGGER[] = {BUTTON1_EXTI_TRIGGER};

uint32_t CORE_FW_Version_SysFlag = 0xFFFFFFFF;
uint32_t NVMEM_SPARK_Reset_SysFlag = 0xFFFFFFFF;
uint32_t FLASH_OTA_Update_SysFlag = 0xFFFFFFFF;
uint32_t OTA_FLASHED_Status_SysFlag = 0xFFFFFFFF;
uint32_t Factory_Reset_SysFlag = 0xFFFFFFFF;

uint32_t WRPR_Value = 0xFFFFFFFF;
uint32_t Flash_Sectors_Protected = 0x0;
uint32_t Internal_Flash_Address = 0;
uint32_t External_Flash_Address = 0;
uint32_t Internal_Flash_Data = 0;
uint8_t External_Flash_Data[4];
uint16_t Flash_Update_Index = 0;
uint32_t EraseCounter = 0;
uint32_t NbrOfPage = 0;
volatile FLASH_Status FLASHStatus = FLASH_COMPLETE;

__IO uint16_t CC3000_SPI_CR;
__IO uint16_t sFLASH_SPI_CR;

/* Extern variables ----------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/**
 * @brief  Initialise Data Watchpoint and Trace Register (DWT).
 * @param  None
 * @retval None
 *
 *
 */

static void DWT_Init(void)
{
    DBGMCU->CR |= DBGMCU_SETTINGS;
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
}

/**
 * @brief  Configures Main system clocks & power.
 * @param  None
 * @retval None
 */
void Set_System(void)
{
    /*!< At this stage the microcontroller clock setting is already configured,
     this is done through SystemInit() function which is called from startup
     file (startup_stm32f10x_xx.S) before to branch to application main.
     To reconfigure the default setting of SystemInit() function, refer to
     system_stm32f10x.c file
     */

    /* Enable PWR and BKP clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Enable write access to Backup domain */
    PWR_BackupAccessCmd(ENABLE);

    /* Should we execute System Standby mode */
    if(RTC_ReadBackupRegister(RTC_BKP_DR9) == 0xA5A5)
    {
        /* Clear Standby mode system flag */
        RTC_WriteBackupRegister(RTC_BKP_DR9, 0xFFFFFFFF);

        /* Request to enter STANDBY mode */
        PWR_EnterSTANDBYMode();

        /* Following code will not be reached */
        while(1);
    }

    DWT_Init();

    /* NVIC configuration */
    NVIC_Configuration();

    /* Configure the LEDs and set the default states */
    int LEDx;
    for(LEDx = 0; LEDx < LEDn; ++LEDx)
    {
        LED_Init(LEDx);
    }

    /* Configure TIM5 for LED-PWM and BUTTON-DEBOUNCE usage */
    UI_Timer_Configure();

    /* Configure GPIO for outlet relays */
    Outlet_GPIO_Init();

    /* Configure the Button */
    BUTTON_Init(BUTTON1, BUTTON_MODE_EXTI);
}

/*******************************************************************************
 * Function Name  : NVIC_Configuration
 * Description    : Configures Vector Table base location.
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void NVIC_Configuration(void)
{
    /* Configure the NVIC Preemption Priority Bits */
    /* 4 bits for pre-emption priority(0-15 PreemptionPriority) and 0 bits for subpriority(0 SubPriority) */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
}

/*******************************************************************************
 * Function Name  : SysTick_Configuration
 * Description    : Setup SysTick Timer and Configure its Interrupt Priority
 * Input          : None
 * Output         : None
 * Return         : None
 *******************************************************************************/
void SysTick_Configuration(void)
{
    /* Setup SysTick Timer for 1 msec interrupts */
    if (SysTick_Config(SystemCoreClock / 1000))
    {
        /* Capture error */
        while (1)
        {
        }
    }

    /* Configure the SysTick Handler Priority: Preemption priority and subpriority */
    NVIC_SetPriority(SysTick_IRQn, SYSTICK_IRQ_PRIORITY);   //OLD: NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0x03, 0x00)
}

/*******************************************************************************
* Function Name  : Delay
* Description    : Inserts a delay time.
* Input          : nTime: specifies the delay time length, in milliseconds.
* Output         : None
* Return         : None
*******************************************************************************/
void Delay(uint32_t nTime)
{
    TimingDelay = nTime;

    while (TimingDelay != 0x00);
}

/*******************************************************************************
 * Function Name  : Delay_Microsecond
 * Description    : Inserts a delay time in microseconds using 32-bit DWT->CYCCNT
 * Input          : uSec: specifies the delay time length, in microseconds.
 * Output         : None
 * Return         : None
 *******************************************************************************/
void Delay_Microsecond(uint32_t uSec)
{
  volatile uint32_t DWT_START = DWT->CYCCNT;

  // keep DWT_TOTAL from overflowing (max 59.652323s w/72MHz SystemCoreClock)
  if (uSec > (UINT_MAX / SYSTEM_US_TICKS))
  {
    uSec = (UINT_MAX / SYSTEM_US_TICKS);
  }

  volatile uint32_t DWT_TOTAL = (SYSTEM_US_TICKS * uSec);

  while((DWT->CYCCNT - DWT_START) < DWT_TOTAL)
  {
    KICK_WDT();
  }
}

void RTC_Configuration(void)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;
    RTC_AlarmTypeDef RTCAlarm_InitStructure;

    RTC_DateTypeDef  RTC_DateStructure;
    RTC_TimeTypeDef  RTC_TimeStructure;
    RTC_InitTypeDef  RTC_InitStructure;

    __IO uint32_t uwAsynchPrediv = 0;
    __IO uint32_t uwSynchPrediv = 0;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

    /* Allow access to BKP Domain */
    PWR_BackupAccessCmd(ENABLE);

    /* Enable LSE */
    RCC_LSEConfig(RCC_LSE_ON);

    /* Select LSE as RTC Clock Source */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

    /* Enable RTC Clock */
    RCC_RTCCLKCmd(ENABLE);

    /* Check if the StandBy flag is set */
    if(RTC_GetFlagStatus(RTC_FLAG_INITS) != RESET)
    {
        /* System resumed from STANDBY mode */

        /* Clear StandBy flag */
        PWR_ClearFlag(PWR_FLAG_SB);

        /* Wait for RTC APB registers synchronisation */
        RTC_WaitForSynchro();

        /* No need to configure the RTC as the RTC configuration(clock source, enable,
           prescaler,...) is kept after wake-up from STANDBY */
    }
    else
    {
        /* StandBy flag is not set, configure RTC. */

        /* Reset Backup Domain */
        RCC_BackupResetCmd(ENABLE);
        RCC_BackupResetCmd(DISABLE);

        /* Enable LSE */
        RCC_LSEConfig(RCC_LSE_ON);

        /* Wait till LSE is ready */
        while (RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
        {
            //Do nothing
        }

        /* Select LSE as RTC Clock Source */
        RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

        /* Enable RTC Clock */
        RCC_RTCCLKCmd(ENABLE);

        /* Wait for RTC registers synchronization */
        RTC_WaitForSynchro();

        uwSynchPrediv = 0xFF;
        uwAsynchPrediv = 0x7F;

        /* Configure the RTC data register and RTC prescaler */
        RTC_InitStructure.RTC_AsynchPrediv = uwAsynchPrediv;
        RTC_InitStructure.RTC_SynchPrediv = uwSynchPrediv;
        RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
        RTC_Init(&RTC_InitStructure);

        /* Configure the RTC WakeUp Clock source: CK_SPRE (1Hz) */
        RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
        //RTC_SetWakeUpCounter(30);
        //RTC_ITConfig(RTC_IT_WUT, ENABLE);

         /* Configure EXTI Line22(RTC WakeUp) to generate an interrupt on rising edge */
        EXTI_ClearITPendingBit(EXTI_Line22);
        EXTI_InitStructure.EXTI_Line = EXTI_Line22;
        EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
        EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
        EXTI_InitStructure.EXTI_LineCmd = ENABLE;
        EXTI_Init(&EXTI_InitStructure);

        /* Enable the RTC Wakeup Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = RTC_IRQ_PRIORITY;
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
        NVIC_Init(&NVIC_InitStructure);
    }
}

void Enter_STANDBY_Mode(void)
{
    /* Execute Standby mode on next system reset */
    RTC_WriteBackupRegister(RTC_BKP_DR9, 0xA5A5);

    /* Reset System */
    NVIC_SystemReset();
}

void IWDG_Reset_Enable(uint32_t msTimeout)
{
    /* Enable write access to IWDG_PR and IWDG_RLR registers */
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);

    /* IWDG counter clock: LSI/256 */
    IWDG_SetPrescaler(IWDG_Prescaler_128);

        /* IWDG timeout may vary due to LSI frequency dispersion */
        msTimeout = ((msTimeout * 32) / 128); // LSI Frequency = 32kHz
        if (msTimeout > 0xfff) msTimeout = 0xfff;

    IWDG_SetReload((uint16_t)msTimeout);

    /* Reload IWDG counter */
    IWDG_ReloadCounter();

    /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
    IWDG_Enable();
}

void UI_Timer_Configure(void)
{
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;

    /* Enable TIM5 clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

    /* TIM5 Update Frequency = 42000000/42/10000 = 100Hz = 10ms */
    /* TIM5_Prescaler: 42 */
    /* TIM5_Autoreload: 9999 -> 100Hz = 10ms */
    uint16_t TIM5_Prescaler = 42;
    uint16_t TIM5_Autoreload = (1000000 / UI_TIMER_FREQUENCY) - 1;

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);

    /* Time Base Configuration */
    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    TIM_TimeBaseStructure.TIM_Period = TIM5_Autoreload;
    TIM_TimeBaseStructure.TIM_Prescaler = TIM5_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0x0000;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM5, &TIM_TimeBaseStructure);

    TIM_OCStructInit(&TIM_OCInitStructure);

    /* PWM1 Mode configuration: Channel 1, 2 and 3 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0x0000;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
    TIM_OCInitStructure.TIM_OCIdleState = TIM_OCIdleState_Set;

    TIM_OC1Init(TIM5, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM5, TIM_OCPreload_Disable);

    TIM_OC2Init(TIM5, &TIM_OCInitStructure);
    TIM_OC2PreloadConfig(TIM5, TIM_OCPreload_Disable);

    TIM_OC3Init(TIM5, &TIM_OCInitStructure);
    TIM_OC3PreloadConfig(TIM5, TIM_OCPreload_Disable);


    /* Output Compare Timing Mode configuration: Channel 4 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0x0000;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC4Init(TIM5, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM5, TIM_OCPreload_Disable);

    GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_TIM5);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_TIM5);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_TIM5);

    TIM_ARRPreloadConfig(TIM5, ENABLE);

    /* TIM5 enable counter */
    TIM_Cmd(TIM5, ENABLE);
}

void Relay_Timer_Configure(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;

    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
    TIM_TimeBaseStructure.TIM_Period = 40 - 1; // 4 MHz down to 100 KHz
    TIM_TimeBaseStructure.TIM_Prescaler = 21 - 1; // Down to 4 MHz
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Inactive;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 26; // 26 * 0.25us = 6.5us on
    TIM_OC1Init(TIM2, &TIM_OCInitStructure);
    TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Disable);
    TIM_ITConfig(TIM2, TIM_IT_Update | TIM_IT_CC1, ENABLE);
    TIM_Cmd(TIM2, ENABLE);
}

void LED_SetRGBColor(uint32_t RGB_Color)
{
    lastRGBColor = RGB_Color;
    LED_TIM_CCR[2] = (uint16_t)((((RGB_Color & 0xFF0000) >> 16) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16); //LED3 -> Red Led
    LED_TIM_CCR[3] = (uint16_t)((((RGB_Color & 0xFF00) >> 8) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16);    //LED4 -> Green Led
    LED_TIM_CCR[1] = (uint16_t)(((RGB_Color & 0xFF) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16);             //LED2 -> Blue Led
}

void LED_SetSignalingColor(uint32_t RGB_Color)
{
    lastSignalColor = RGB_Color;
    LED_TIM_CCR_SIGNAL[2] = (uint16_t)((((RGB_Color & 0xFF0000) >> 16) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16); //LED3 -> Red Led
    LED_TIM_CCR_SIGNAL[3] = (uint16_t)((((RGB_Color & 0xFF00) >> 8) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16);    //LED4 -> Green Led
    LED_TIM_CCR_SIGNAL[1] = (uint16_t)(((RGB_Color & 0xFF) * LED_RGB_BRIGHTNESS * (TIM5->ARR + 1)) >> 16);             //LED2 -> Blue Led
}

void LED_Signaling_Start(void)
{
    LED_RGB_OVERRIDE = 1;

    LED_Off(LED_RGB);
}

void LED_Signaling_Stop(void)
{
    LED_RGB_OVERRIDE = 0;

    LED_On(LED_RGB);
}

void LED_SetBrightness(uint8_t brightness)
{
    LED_RGB_BRIGHTNESS = brightness;

    /* Recompute RGB scale using new value for brightness. */
    if (LED_RGB_OVERRIDE)
        LED_SetSignalingColor(lastSignalColor);
    else
        LED_SetRGBColor(lastRGBColor);
}

/**
  * @brief  Configures LED GPIO.
  * @param  Led: Specifies the Led to be configured.
  *   This parameter can be one of following parameters:
  *     @arg LED2, LED3, LED4
  * @retval None
  */
void LED_Init(Led_TypeDef Led)
{
    GPIO_InitTypeDef  GPIO_InitStructure;

    /* Enable the GPIO_LED Clock */
    RCC_AHB1PeriphClockCmd(LED_GPIO_CLK[Led], ENABLE);

    /* Configure the GPIO_LED pin as alternate function push-pull */
    GPIO_InitStructure.GPIO_Pin = LED_GPIO_PIN[Led];
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_Init(LED_GPIO_PORT[Led], &GPIO_InitStructure);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on.
  *   This parameter can be one of following parameters:
  *     @arg LED2, LED_USER, LED_RGB
  * @retval None
  */
void LED_On(Led_TypeDef Led)
{
    switch(Led)
    {
    case LED_RGB:    //LED_SetRGBColor() should be called first for this Case
     if(LED_RGB_OVERRIDE == 0)
     {
         TIM5->CCR2 = LED_TIM_CCR[2];
         TIM5->CCR3 = LED_TIM_CCR[3];
         TIM5->CCR1 = LED_TIM_CCR[1];
     }
     else
     {
         TIM5->CCR2 = LED_TIM_CCR_SIGNAL[2];
         TIM5->CCR3 = LED_TIM_CCR_SIGNAL[3];
         TIM5->CCR1 = LED_TIM_CCR_SIGNAL[1];
     }

     led_fade_step = NUM_LED_FADE_STEPS - 1;
     led_fade_direction = -1; /* next fade is falling */
     break;
    default:
        break;
    }
}

/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off.
  *   This parameter can be one of following parameters:
  *     @arg LED1, LED2, LED_USER, LED_RGB
  * @retval None
  */
void LED_Off(Led_TypeDef Led)
{
    switch(Led)
    {
    case LED_RGB:
        TIM5->CCR2 = 0;
        TIM5->CCR3 = 0;
        TIM5->CCR1 = 0;
        led_fade_step = 0;
        led_fade_direction = 1; /* next fade is rising. */
        break;
    default:
        break;
    }
}

/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled.
  *   This parameter can be one of following parameters:
  *     @arg LED2, LED_USER, LED_RGB
  * @retval None
  */
void LED_Toggle(Led_TypeDef Led)
{
    switch(Led)
    {
    case LED_RGB://LED_SetRGBColor() and LED_On() should be called first for this Case
        if(LED_RGB_OVERRIDE == 0)
        {
            if (TIM5->CCR2)
                TIM5->CCR2 = 0;
            else
                TIM5->CCR2 = LED_TIM_CCR[2];

            if (TIM5->CCR3)
                TIM5->CCR3 = 0;
            else
                TIM5->CCR3 = LED_TIM_CCR[3];

            if (TIM5->CCR1)
                TIM5->CCR1 = 0;
            else
                TIM5->CCR1 = LED_TIM_CCR[1];
        }
        else
        {
            if (TIM5->CCR2)
                TIM5->CCR2 = 0;
            else
                TIM5->CCR2 = LED_TIM_CCR_SIGNAL[2];

            if (TIM5->CCR3)
                TIM5->CCR3 = 0;
            else
                TIM5->CCR3 = LED_TIM_CCR_SIGNAL[3];

            if (TIM5->CCR1)
                TIM5->CCR1 = 0;
            else
                TIM5->CCR1 = LED_TIM_CCR_SIGNAL[1];
        }
        break;

    default:
        break;
    }
}

/**
  * @brief  Fades selected LED.
  * @param  Led: Specifies the Led to be set on.
  *   This parameter can be one of following parameters:
  *     @arg LED2, LED_RGB
  * @retval None
  */
void LED_Fade(Led_TypeDef Led)
{
    /* Update position in fade. */
    if (led_fade_step == 0)
        led_fade_direction = 1; /* Switch to fade growing. */
    else if (led_fade_step == NUM_LED_FADE_STEPS - 1)
        led_fade_direction = -1; /* Switch to fade falling. */

    led_fade_step += led_fade_direction;

    if(Led == LED_RGB)
    {
        if(LED_RGB_OVERRIDE == 0)
        {
            TIM5->CCR2 = (((uint32_t) LED_TIM_CCR[2]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
            TIM5->CCR3 = (((uint32_t) LED_TIM_CCR[3]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
            TIM5->CCR1 = (((uint32_t) LED_TIM_CCR[1]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
        }
        else
        {
            TIM5->CCR2 = (((uint32_t) LED_TIM_CCR_SIGNAL[2]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
            TIM5->CCR3 = (((uint32_t) LED_TIM_CCR_SIGNAL[3]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
            TIM5->CCR1 = (((uint32_t) LED_TIM_CCR_SIGNAL[1]) * led_fade_step) / (NUM_LED_FADE_STEPS - 1);
        }
    }
}

/**
  * @brief  Sets the RTC time
  * @param  t: rtc_time structure with the time to be set
  *   This parameter has hours, minutes, and seconds fields.
  * @retval None
  */
#ifdef __cplusplus
extern "C"
#endif
void SetRTCTime(rtc_time t)
{
    RTC_TimeTypeDef RTC_TimeStruct;

    RTC_TimeStruct.RTC_Hours = t.hours;
    RTC_TimeStruct.RTC_Minutes = t.minutes;
    RTC_TimeStruct.RTC_Seconds = t.seconds;
    RTC_SetTime(RTC_Format_BIN, &RTC_TimeStruct);

    RTC_TimeTypeDef RTC_TimeStruct2;
    RTC_GetTime(RTC_Format_BIN, &RTC_TimeStruct2);

    int i = 0;
}

/**
  * @brief  Sets the RTC date
  * @param  d: rtc_date structure with the time to be set
  *   This parameter has weekday, month, day, and year fields.
  * @retval None
  */
void SetRTCDate(rtc_date d)
{
    RTC_DateTypeDef RTC_DateStruct;
    RTC_DateStruct.RTC_WeekDay = d.weekday;
    RTC_DateStruct.RTC_Month = d.month;
    RTC_DateStruct.RTC_Date = d.day;
    RTC_DateStruct.RTC_Year = d.year;
    RTC_SetDate(RTC_Format_BIN, &RTC_DateStruct);
}

/**
  * @brief  Gets the RTC time
  * @param  None
  * @retval rtc_time stucture
  *   This parameter has hours, minutes, and seconds fields.
  */
rtc_time GetRTCTime(void)
{
    rtc_time t;

    RTC_TimeTypeDef RTC_TimeStruct;
    RTC_GetTime(RTC_Format_BIN, &RTC_TimeStruct);

    t.hours = RTC_TimeStruct.RTC_Hours;
    t.minutes = RTC_TimeStruct.RTC_Minutes;
    t.seconds = RTC_TimeStruct.RTC_Seconds;

    return t;
}

/**
  * @brief  Gets the RTC date
  * @param  None
  * @retval rtc_date stucture
  *   This parameter has weekday, month, day, and year fields.
  */
rtc_date GetRTCDate(void)
{
    rtc_date d;
    RTC_DateTypeDef RTC_DateStruct;
    RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);

    d.weekday = RTC_DateStruct.RTC_WeekDay;
    d.month = RTC_DateStruct.RTC_Month;
    d.day = RTC_DateStruct.RTC_Date;
    d.year = RTC_DateStruct.RTC_Year;

    return d;
}

/**
  * @brief  Configures Button GPIO, EXTI Line and DEBOUNCE Timer.
  * @param  Button: Specifies the Button to be configured.
  *   This parameter can be one of following parameters:
  *     @arg BUTTON1: Button1
  * @param  Button_Mode: Specifies Button mode.
  *   This parameter can be one of following parameters:
  *     @arg BUTTON_MODE_GPIO: Button will be used as simple IO
  *     @arg BUTTON_MODE_EXTI: Button will be connected to EXTI line with interrupt
  *                     generation capability
  * @retval None
  */
void BUTTON_Init(Button_TypeDef Button, ButtonMode_TypeDef Button_Mode)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable the BUTTON Clock */
    RCC_AHB1PeriphClockCmd(BUTTON_GPIO_CLK[Button], ENABLE);

    /* Configure Button pin as input floating */
    GPIO_InitStructure.GPIO_Pin = BUTTON_GPIO_PIN[Button];
    GPIO_InitStructure.GPIO_Mode = BUTTON_GPIO_MODE[Button];
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;   // this sets the pin type to push / pull (as opposed to open drain)
    GPIO_InitStructure.GPIO_PuPd = BUTTON_GPIO_PUPD[Button];
    GPIO_Init(BUTTON_GPIO_PORT[Button], &GPIO_InitStructure);

    if (Button_Mode == BUTTON_MODE_EXTI)
    {
        /* Disable TIM1 CC4 Interrupt */
        TIM_ITConfig(TIM5, TIM_IT_CC4, DISABLE);

        /* Enable the TIM1 Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = TIM5_IRQn;
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = TIM5_CC_IRQ_PRIORITY;    //OLD: 0x02
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;                           //OLD: 0x00
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

        NVIC_Init(&NVIC_InitStructure);

        /* Enable the Button EXTI Interrupt */
        NVIC_InitStructure.NVIC_IRQChannel = BUTTON_IRQn[Button];
        NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EXTI9_5_IRQ_PRIORITY;      //OLD: 0x02
        NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;                           //OLD: 0x01
        NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

        NVIC_Init(&NVIC_InitStructure);

        BUTTON_EXTI_Config(Button, ENABLE);
    }
}

void BUTTON_EXTI_Config(Button_TypeDef Button, FunctionalState NewState)
{
    EXTI_InitTypeDef EXTI_InitStructure;

    /* Connect EXTI Line to appropriate GPIO Pin */
    SYSCFG_EXTILineConfig(BUTTON_GPIO_PORT_SOURCE[Button], BUTTON_GPIO_PIN_SOURCE[Button]);

    /* Clear the EXTI line pending flag */  
    EXTI_ClearFlag(BUTTON_EXTI_LINE[Button]);

    /* Configure Button EXTI line */
    EXTI_InitStructure.EXTI_Line = BUTTON_EXTI_LINE[Button];
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = BUTTON_EXTI_TRIGGER[Button];
    EXTI_InitStructure.EXTI_LineCmd = NewState;
    EXTI_Init(&EXTI_InitStructure);
}

/**
  * @brief  Returns the selected Button non-filtered state.
  * @param  Button: Specifies the Button to be checked.
  *   This parameter can be one of following parameters:
  *     @arg BUTTON1: Button1
  * @retval Actual Button Pressed state.
  */
uint8_t BUTTON_GetState(Button_TypeDef Button)
{
    return GPIO_ReadInputDataBit(BUTTON_GPIO_PORT[Button], BUTTON_GPIO_PIN[Button]);
}

/**
  * @brief  Returns the selected Button Debounced Time.
  * @param  Button: Specifies the Button to be checked.
  *   This parameter can be one of following parameters:
  *     @arg BUTTON1: Button1
  * @retval Button Debounced time in millisec.
  */
uint16_t BUTTON_GetDebouncedTime(Button_TypeDef Button)
{
    return BUTTON_DEBOUNCED_TIME[Button];
}

void BUTTON_ResetDebouncedState(Button_TypeDef Button)
{
    BUTTON_DEBOUNCED_TIME[Button] = 0;
}

/**
 * @brief  Initialize the CC3000 - CS and ENABLE lines.
 * @param  None
 * @retval None
 */
void CC3000_WIFI_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* CC3000_WIFI_CS_GPIO and CC3000_WIFI_EN_GPIO Peripheral clock enable */
    RCC_AHB1PeriphClockCmd(CC3000_WIFI_CS_GPIO_CLK | CC3000_WIFI_EN_GPIO_CLK, ENABLE);

    /* Configure CC3000_WIFI pins: CS */
    GPIO_InitStructure.GPIO_Pin = CC3000_WIFI_CS_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(CC3000_WIFI_CS_GPIO_PORT, &GPIO_InitStructure);

    /* Deselect CC3000 */
    CC3000_CS_HIGH();

    /* Configure CC3000_WIFI pins: Enable */
    GPIO_InitStructure.GPIO_Pin = CC3000_WIFI_EN_GPIO_PIN;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
    GPIO_Init(CC3000_WIFI_EN_GPIO_PORT, &GPIO_InitStructure);

    /* Disable CC3000 */
    CC3000_Write_Enable_Pin(WLAN_DISABLE);
}

/**
 * @brief  Initialize and configure the SPI peripheral used by CC3000.
 * @param  None
 * @retval None
 */
void CC3000_SPI_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef SPI_InitStructure;

    /* CC3000_SPI_SCK_GPIO, CC3000_SPI_MOSI_GPIO and CC3000_SPI_MISO_GPIO Peripheral clock enable */
    RCC_AHB1PeriphClockCmd(CC3000_SPI_SCK_GPIO_CLK 
                            | CC3000_SPI_MOSI_GPIO_CLK
                            | CC3000_SPI_MISO_GPIO_CLK, ENABLE);

    /* CC3000_SPI Peripheral clock enable */
    CC3000_SPI_CLK_CMD(CC3000_SPI_CLK, ENABLE);

    /* Configure CC3000_SPI pins: SCK */
    GPIO_InitStructure.GPIO_Pin = CC3000_SPI_SCK_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(CC3000_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    /* Configure CC3000_SPI pins: MOSI */
    GPIO_InitStructure.GPIO_Pin = CC3000_SPI_MOSI_GPIO_PIN;
    GPIO_Init(CC3000_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    /* Configure CC3000_SPI pins: MISO */
    GPIO_InitStructure.GPIO_Pin = CC3000_SPI_MISO_GPIO_PIN;
    GPIO_Init(CC3000_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    GPIO_PinAFConfig(CC3000_SPI_SCK_GPIO_PORT, CC3000_SPI_SCK_GPIO_PIN_SOURCE, GPIO_AF_SPI2);
    GPIO_PinAFConfig(CC3000_SPI_MOSI_GPIO_PORT, CC3000_SPI_MOSI_GPIO_PIN_SOURCE, GPIO_AF_SPI2);
    GPIO_PinAFConfig(CC3000_SPI_MISO_GPIO_PORT, CC3000_SPI_MISO_GPIO_PIN_SOURCE, GPIO_AF_SPI2);

    /* CC3000_SPI Config */
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = CC3000_SPI_BAUDRATE_PRESCALER;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(CC3000_SPI, &SPI_InitStructure);

    CC3000_SPI_CR = CC3000_SPI->CR1;
}

/**
 * @brief  Configure the DMA Peripheral used to handle CC3000 communication via SPI.
 * @param  None
 * @retval None
 */
void CC3000_DMA_Config(CC3000_DMADirection_TypeDef Direction, uint8_t* buffer, uint16_t NumData)
{
    DMA_InitTypeDef DMA_InitStructure;

    RCC_AHB1PeriphClockCmd(CC3000_SPI_DMA_CLK, ENABLE);

    DMA_InitStructure.DMA_PeripheralBaseAddr = CC3000_SPI_DR_BASE;
    /* ADDED */
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    /* END ADDED */
    DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t) buffer;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;

    /* DMA used for Reception */
    if (Direction == CC3000_DMA_RX)
    {
        DMA_InitStructure.DMA_Channel = CC3000_SPI_RX_DMA_CHANNEL;
        DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
        DMA_InitStructure.DMA_BufferSize = NumData;
        DMA_DeInit(CC3000_SPI_RX_DMA_STREAM );
        DMA_Init(CC3000_SPI_RX_DMA_STREAM, &DMA_InitStructure);
    }
    /* DMA used for Transmission */
    else if (Direction == CC3000_DMA_TX)
    {
        DMA_InitStructure.DMA_Channel = CC3000_SPI_TX_DMA_CHANNEL;
        DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
        DMA_InitStructure.DMA_BufferSize = NumData;
        DMA_DeInit(CC3000_SPI_TX_DMA_STREAM );
        DMA_Init(CC3000_SPI_TX_DMA_STREAM, &DMA_InitStructure);
    }
}

void CC3000_SPI_DMA_Init(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Configure and enable SPI DMA TX Channel interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = CC3000_SPI_TX_DMA_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = DMA1_STREAM4_IRQ_PRIORITY;   //OLD: 0x00
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;                               //OLD: 0x00
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    CC3000_SPI_Init();

    /* Enable SPI DMA TX Channel Transfer Complete Interrupt */
    DMA_ITConfig(CC3000_SPI_TX_DMA_STREAM, DMA_IT_TC, ENABLE);

    /* Enable SPI DMA request */
    SPI_I2S_DMACmd(CC3000_SPI, SPI_I2S_DMAReq_Rx, ENABLE);
    SPI_I2S_DMACmd(CC3000_SPI, SPI_I2S_DMAReq_Tx, ENABLE);

    /* Enable CC3000_SPI */
    SPI_Cmd(CC3000_SPI, ENABLE);
}

void CC3000_SPI_DMA_Streams(FunctionalState NewState)
{
    /* Enable/Disable DMA RX Stream */
    DMA_Cmd(CC3000_SPI_RX_DMA_STREAM, NewState);
    /* Enable/Disable DMA TX Stream */
    DMA_Cmd(CC3000_SPI_TX_DMA_STREAM, NewState);
}

/* Select CC3000: ChipSelect pin low */
void CC3000_CS_LOW(void)
{
    acquire_spi_bus(BUS_OWNER_CC3000);
    CC3000_SPI->CR1 &= ((uint16_t)0xFFBF);
    CC3000_SPI->CR1 = CC3000_SPI_CR | ((uint16_t)0x0040);
    GPIO_ResetBits(CC3000_WIFI_CS_GPIO_PORT, CC3000_WIFI_CS_GPIO_PIN);
}

/* Deselect CC3000: ChipSelect pin high */
void CC3000_CS_HIGH(void)
{
    GPIO_SetBits(CC3000_WIFI_CS_GPIO_PORT, CC3000_WIFI_CS_GPIO_PIN);
    release_spi_bus(BUS_OWNER_CC3000);
}

/* CC3000 Hardware related callbacks passed to wlan_init */
long CC3000_Read_Interrupt_Pin(void)
{
    return GPIO_ReadInputDataBit(CC3000_WIFI_INT_GPIO_PORT, CC3000_WIFI_INT_GPIO_PIN );
}

void CC3000_Interrupt_Enable(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* CC3000_WIFI_INT_GPIO clock enable */
    RCC_AHB1PeriphClockCmd(CC3000_WIFI_INT_GPIO_CLK, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Configure CC3000_WIFI pins: Interrupt */
    GPIO_InitStructure.GPIO_Pin = CC3000_WIFI_INT_GPIO_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(CC3000_WIFI_INT_GPIO_PORT, &GPIO_InitStructure);

    /* Select the CC3000_WIFI_INT GPIO pin used as EXTI Line */
    SYSCFG_EXTILineConfig(CC3000_WIFI_INT_EXTI_PORT_SOURCE, CC3000_WIFI_INT_EXTI_PIN_SOURCE);

    /* Clear the EXTI line pending flag */
    EXTI_ClearFlag(CC3000_WIFI_INT_EXTI_LINE);

    /* Configure and Enable CC3000_WIFI_INT EXTI line */
    EXTI_InitStructure.EXTI_Line = CC3000_WIFI_INT_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable and set CC3000_WIFI_INT EXTI Interrupt to the lowest priority */
    NVIC_InitStructure.NVIC_IRQChannel = CC3000_WIFI_INT_EXTI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = EXTI15_10_IRQ_PRIORITY;      //OLD: 0x00
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;                               //OLD: 0x01
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void CC3000_Interrupt_Disable(void)
{
    EXTI_InitTypeDef EXTI_InitStructure;
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Disable CC3000_WIFI_INT EXTI Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = CC3000_WIFI_INT_EXTI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);

    /* Disable CC3000_WIFI_INT EXTI line */
    EXTI_InitStructure.EXTI_Line = CC3000_WIFI_INT_EXTI_LINE;
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_InitStructure);
}

void CC3000_Write_Enable_Pin(unsigned char val)
{
    /* Set WLAN Enable/Disable */
    if (val != WLAN_DISABLE)
    {
        GPIO_SetBits(CC3000_WIFI_EN_GPIO_PORT, CC3000_WIFI_EN_GPIO_PIN );
    }
    else
    {
        GPIO_ResetBits(CC3000_WIFI_EN_GPIO_PORT, CC3000_WIFI_EN_GPIO_PIN );
    }
}

/**
  * @brief  DeInitializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void sFLASH_SPI_DeInit(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Disable the sFLASH_SPI  */
    SPI_Cmd(sFLASH_SPI, DISABLE);

    /* DeInitializes the sFLASH_SPI */
    SPI_I2S_DeInit(sFLASH_SPI);

    /* sFLASH_SPI Peripheral clock disable */
    sFLASH_SPI_CLK_CMD(sFLASH_SPI_CLK, DISABLE);

    /* Configure sFLASH_SPI SCK to default values
     * (input, 2MHz, Push-Pull, No pullup) */ 
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_SCK_GPIO_PIN;
    GPIO_Init(sFLASH_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_SPI MISO to default values */
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_MISO_GPIO_PIN;
    GPIO_Init(sFLASH_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_SPI MOSI to default values */
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_MOSI_GPIO_PIN;
    GPIO_Init(sFLASH_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_SPI CS to default values */
    GPIO_InitStructure.GPIO_Pin = sFLASH_MEM_CS_GPIO_PIN;
    GPIO_Init(sFLASH_MEM_CS_GPIO_PORT, &GPIO_InitStructure);
}

/**
  * @brief  Initializes the peripherals used by the SPI FLASH driver.
  * @param  None
  * @retval None
  */
void sFLASH_SPI_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
    SPI_InitTypeDef  SPI_InitStructure;

    /* sFLASH_MEM_CS_GPIO, sFLASH_SPI_MOSI_GPIO, sFLASH_SPI_MISO_GPIO
       and sFLASH_SPI_SCK_GPIO Periph clock enable */
    RCC_AHB1PeriphClockCmd(sFLASH_MEM_CS_GPIO_CLK
                            | sFLASH_SPI_MOSI_GPIO_CLK
                            | sFLASH_SPI_MISO_GPIO_CLK
                            | sFLASH_SPI_SCK_GPIO_CLK, ENABLE);

    /* sFLASH_SPI Periph clock enable */
    sFLASH_SPI_CLK_CMD(sFLASH_SPI_CLK, ENABLE);

    /* Configure sFLASH_SPI pins: SCK */
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_SCK_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(sFLASH_SPI_SCK_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_SPI pins: MOSI */
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_MOSI_GPIO_PIN;
    GPIO_Init(sFLASH_SPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_SPI pins: MISO */
    GPIO_InitStructure.GPIO_Pin = sFLASH_SPI_MISO_GPIO_PIN;
    GPIO_Init(sFLASH_SPI_MISO_GPIO_PORT, &GPIO_InitStructure);

    /* Configure sFLASH_MEM_CS_GPIO_PIN pin: sFLASH CS pin */
    GPIO_InitStructure.GPIO_Pin = sFLASH_MEM_CS_GPIO_PIN;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
    GPIO_Init(sFLASH_MEM_CS_GPIO_PORT, &GPIO_InitStructure);

    /* Connect SPI2 pins to SPI alternate function */
    GPIO_PinAFConfig(GPIOB, sFLASH_SPI_SCK_GPIO_PIN_SOURCE, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, sFLASH_SPI_MOSI_GPIO_PIN_SOURCE, GPIO_AF_SPI2);
    GPIO_PinAFConfig(GPIOB, sFLASH_SPI_MISO_GPIO_PIN_SOURCE, GPIO_AF_SPI2);

    /*!< Deselect the FLASH: Chip Select high */
    sFLASH_CS_HIGH();

    /*!< SPI configuration */
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = sFLASH_SPI_BAUDRATE_PRESCALER;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(sFLASH_SPI, &SPI_InitStructure);

    sFLASH_SPI_CR = sFLASH_SPI->CR1;

    /*!< Enable the sFLASH_SPI  */
    SPI_Cmd(sFLASH_SPI, ENABLE);
}

/* Select sFLASH: Chip Select pin low */
void sFLASH_CS_LOW(void)
{
    acquire_spi_bus(BUS_OWNER_SFLASH);
    sFLASH_SPI->CR1 &= ((uint16_t)0xFFBF);
    sFLASH_SPI->CR1 = sFLASH_SPI_CR | ((uint16_t)0x0040);
    GPIO_ResetBits(sFLASH_MEM_CS_GPIO_PORT, sFLASH_MEM_CS_GPIO_PIN);
}

/* Deselect sFLASH: Chip Select pin high */
void sFLASH_CS_HIGH(void)
{
    GPIO_SetBits(sFLASH_MEM_CS_GPIO_PORT, sFLASH_MEM_CS_GPIO_PIN);
    release_spi_bus(BUS_OWNER_SFLASH);
    handle_spi_request();
}


/*******************************************************************************
* Function Name  : Set_USBClock
* Description    : Configures USB Clock input (48MHz)
* Input          : None.
* Return         : None.
*******************************************************************************/
void Set_USBClock(void)
{
    /* Enable the USB clock */
    RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_OTG_FS, ENABLE);
}

void Load_SystemFlags(void)
{
    uint32_t Address = SYSTEM_FLAGS_ADDRESS;

    if(!USE_SYSTEM_FLAGS)
        return;

    CORE_FW_Version_SysFlag = (*(__IO uint32_t*) Address);
    Address += 4;

    NVMEM_SPARK_Reset_SysFlag = (*(__IO uint32_t*) Address);
    Address += 4;

    FLASH_OTA_Update_SysFlag = (*(__IO uint32_t*) Address);
    Address += 4;

    OTA_FLASHED_Status_SysFlag = (*(__IO uint32_t*) Address);
    Address += 4;

    Factory_Reset_SysFlag = (*(__IO uint32_t*) Address);
    Address += 4;
}

void Save_SystemFlags(void)
{
    uint32_t Address = SYSTEM_FLAGS_ADDRESS;
    FLASH_Status FLASHStatus = FLASH_COMPLETE;

    if(!USE_SYSTEM_FLAGS)
        return;

    /* Unlock the Flash Program Erase Controller */
    FLASH_Unlock();

    /* Clear All pending flags */
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR 
        | FLASH_FLAG_PGSERR | FLASH_FLAG_WRPERR);

    /* Erase the Internal Flash pages */
    FLASHStatus = FLASH_EraseSector(SYSTEM_FLAGS_SECTOR, VoltageRange_3);
    while(FLASHStatus != FLASH_COMPLETE);

    /* Program CORE_FW_Version_SysFlag */
    FLASHStatus = FLASH_ProgramWord(Address, CORE_FW_Version_SysFlag);
    while(FLASHStatus != FLASH_COMPLETE);
    Address += 4;

    /* Program NVMEM_SPARK_Reset_SysFlag */
    FLASHStatus = FLASH_ProgramWord(Address, NVMEM_SPARK_Reset_SysFlag);
    while(FLASHStatus != FLASH_COMPLETE);
    Address += 4;

    /* Program FLASH_OTA_Update_SysFlag */
    FLASHStatus = FLASH_ProgramWord(Address, FLASH_OTA_Update_SysFlag);
    while(FLASHStatus != FLASH_COMPLETE);
    Address += 4;

    /* Program OTA_FLASHED_Status_SysFlag */
    FLASHStatus = FLASH_ProgramWord(Address, OTA_FLASHED_Status_SysFlag);
    while(FLASHStatus != FLASH_COMPLETE);
    Address += 4;

    /* Program Factory_Reset_SysFlag */
    FLASHStatus = FLASH_ProgramWord(Address, Factory_Reset_SysFlag);
    while(FLASHStatus != FLASH_COMPLETE);
    Address += 4;

    /* Locks the FLASH Program Erase Controller */
    FLASH_Lock();
}

void FLASH_WriteProtection_Enable(uint32_t FLASH_Sectors)
{
    /* Enable the FLASH option control register access */
    FLASH_OB_Unlock();

    /* Get pages write protection status */
    WRPR_Value = FLASH_OB_GetWRP();

    /* Check if desired pages are not yet write protected */
    if(((~WRPR_Value) & FLASH_Sectors ) != FLASH_Sectors)
    {
        /* Get current write protected pages and the new pages to be protected */
        Flash_Sectors_Protected =  (~WRPR_Value) | FLASH_Sectors;

        /* Erase all the option Bytes because if a program operation is
          performed on a protected page, the Flash memory returns a
          protection error */
        //FLASHStatus = FLASH_EraseOptionBytes();

        /* Enable the pages write protection */
        FLASH_OB_WRPConfig(Flash_Sectors_Protected, ENABLE);

        /* Launch the Option Bytes programming process. */
        FLASH_OB_Launch();

        /* Generate System Reset to load the new option byte values */
        //NVIC_SystemReset();
    }

    /* Disable the FLASH option control register access */
    FLASH_OB_Lock();
}

void FLASH_WriteProtection_Disable(uint32_t FLASH_Sectors)
{
    /* Enable the FLASH option control register access */
    FLASH_OB_Unlock();

    /* Enable the pages write protection */
    FLASH_OB_WRPConfig(FLASH_Sectors, DISABLE);

    /* Launch the Option Bytes programming process. */
    FLASH_OB_Launch();

    /* Disable the FLASH option control register access */
    FLASH_OB_Lock();

    /* Generate System Reset to load the new option byte values */
    //NVIC_SystemReset();
}

void FLASH_Erase(void)
{
    FLASHStatus = FLASH_COMPLETE;

    /* Unlock the Flash Program Erase Controller */
    FLASH_Unlock();

    /* Clear All pending flags */
    FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR 
        | FLASH_FLAG_PGSERR | FLASH_FLAG_WRPERR);

    /* TODO: Come up with a cleaner method here... */
    /* Erase the Internal Flash (FW is sectors 3-11) */
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_3, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_4, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_5, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_6, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_7, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_8, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_9, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_10, VoltageRange_3);
    FLASHStatus = FLASH_EraseSector(FLASH_Sector_11, VoltageRange_3);

    /* Locks the FLASH Program Erase Controller */
    FLASH_Lock();
}

void FLASH_Backup(uint32_t sFLASH_Address)
{
#ifdef SPARK_SFLASH_ENABLE

    /* Initialize SPI Flash */
    sFLASH_Init();

    /* Define the number of External Flash pages to be erased */
    NbrOfPage = EXTERNAL_FLASH_BLOCK_SIZE / sFLASH_PAGESIZE;

    /* Erase the SPI Flash pages */
    for (EraseCounter = 0; (EraseCounter < NbrOfPage); EraseCounter++)
    {
        sFLASH_EraseSector(sFLASH_Address + (sFLASH_PAGESIZE * EraseCounter));
    }

    Internal_Flash_Address = CORE_FW_ADDRESS;
    External_Flash_Address = sFLASH_Address;

    /* Program External Flash */
    while (Internal_Flash_Address < INTERNAL_FLASH_END_ADDRESS)
    {
        /* Read data from Internal Flash memory */
        Internal_Flash_Data = (*(__IO uint32_t*) Internal_Flash_Address);
        Internal_Flash_Address += 4;

        /* Program Word to SPI Flash memory */
        External_Flash_Data[0] = (uint8_t)(Internal_Flash_Data & 0xFF);
        External_Flash_Data[1] = (uint8_t)((Internal_Flash_Data & 0xFF00) >> 8);
        External_Flash_Data[2] = (uint8_t)((Internal_Flash_Data & 0xFF0000) >> 16);
        External_Flash_Data[3] = (uint8_t)((Internal_Flash_Data & 0xFF000000) >> 24);
        //OR
        //*((uint32_t *)External_Flash_Data) = Internal_Flash_Data;
        sFLASH_WriteBuffer(External_Flash_Data, External_Flash_Address, 4);
        External_Flash_Address += 4;
    }

#endif
}

/* TODO: This function needs to be updated for STM32F4 flash architecture */
void FLASH_Restore(uint32_t sFLASH_Address)
{
#ifdef SPARK_SFLASH_ENABLE

    /* Initialize SPI Flash */
    sFLASH_Init();

    FLASH_Erase();

    Internal_Flash_Address = CORE_FW_ADDRESS;
    External_Flash_Address = sFLASH_Address;

    /* Unlock the Flash Program Erase Controller */
    FLASH_Unlock();

    /* Program Internal Flash Bank1 */
    while ((Internal_Flash_Address < INTERNAL_FLASH_END_ADDRESS) && (FLASHStatus == FLASH_COMPLETE))
    {
        /* Read data from SPI Flash memory */
        sFLASH_ReadBuffer(External_Flash_Data, External_Flash_Address, 4);
        External_Flash_Address += 4;

        /* Program Word to Internal Flash memory */
        Internal_Flash_Data = (uint32_t)(External_Flash_Data[0] | (External_Flash_Data[1] << 8) | (External_Flash_Data[2] << 16) | (External_Flash_Data[3] << 24));
        //OR
        //Internal_Flash_Data = *((uint32_t *)External_Flash_Data);
        FLASHStatus = FLASH_ProgramWord(Internal_Flash_Address, Internal_Flash_Data);
        Internal_Flash_Address += 4;
    }

    /* Locks the FLASH Program Erase Controller */
    FLASH_Lock();

#endif
}

void FLASH_Begin(uint32_t sFLASH_Address)
{
#ifdef SPARK_SFLASH_ENABLE

    LED_SetRGBColor(RGB_COLOR_MAGENTA);
    LED_On(LED_RGB);

    OTA_FLASHED_Status_SysFlag = 0x0000;
    //FLASH_OTA_Update_SysFlag = 0x5555;
    Save_SystemFlags();
    //BKP_WriteBackupRegister(BKP_DR10, 0x5555);

    Flash_Update_Index = 0;
    External_Flash_Address = sFLASH_Address;

    /* Define the number of External Flash pages to be erased */
    NbrOfPage = EXTERNAL_FLASH_BLOCK_SIZE / sFLASH_PAGESIZE;

    /* Erase the SPI Flash pages */
    for (EraseCounter = 0; (EraseCounter < NbrOfPage); EraseCounter++)
    {
        sFLASH_EraseSector(sFLASH_Address + (sFLASH_PAGESIZE * EraseCounter));
    }

#endif
}

uint16_t FLASH_Update(uint8_t *pBuffer, uint32_t bufferSize)
{
#ifdef SPARK_SFLASH_ENABLE

    uint8_t *writeBuffer = pBuffer;
    uint8_t readBuffer[bufferSize];

    /* Write Data Buffer to SPI Flash memory */
    sFLASH_WriteBuffer(writeBuffer, External_Flash_Address, bufferSize);

    /* Read Data Buffer from SPI Flash memory */
    sFLASH_ReadBuffer(readBuffer, External_Flash_Address, bufferSize);

    /* Is the Data Buffer successfully programmed to SPI Flash memory */
    if (0 == memcmp(writeBuffer, readBuffer, bufferSize))
    {
        External_Flash_Address += bufferSize;
        Flash_Update_Index += 1;
    }
    else
    {
        /* Erase the problematic SPI Flash pages and back off the chunk index */
        External_Flash_Address = ((uint32_t)(External_Flash_Address / sFLASH_PAGESIZE)) * sFLASH_PAGESIZE;
        sFLASH_EraseSector(External_Flash_Address);
        Flash_Update_Index = (uint16_t)((External_Flash_Address - EXTERNAL_FLASH_OTA_ADDRESS) / bufferSize);
    }

    LED_Toggle(LED_RGB);

    return Flash_Update_Index;

#endif
}

void FLASH_End(void)
{
    FLASH_OTA_Update_SysFlag = 0x0005;
    Save_SystemFlags();

    RTC_WriteBackupRegister(RTC_BKP_DR10, 0x0005);

    NVIC_SystemReset();
}

void FLASH_Read_ServerAddress(ServerAddress *server_addr)
{
  uint8_t buf[EXTERNAL_FLASH_SERVER_DOMAIN_LENGTH];
  sFLASH_ReadBuffer(buf,
      EXTERNAL_FLASH_SERVER_DOMAIN_ADDRESS,
      EXTERNAL_FLASH_SERVER_DOMAIN_LENGTH);

  // Internet address stored on external flash may be
  // either a domain name or an IP address.
  // It's stored in a type-length-value encoding.
  // First byte is type, second byte is length, the rest is value.

  switch (buf[0])
  {
    case IP_ADDRESS:
      server_addr->addr_type = IP_ADDRESS;
      server_addr->ip = (buf[2] << 24) | (buf[3] << 16) |
                        (buf[4] << 8)  |  buf[5];
      break;

    case DOMAIN_NAME:
      if (buf[1] <= EXTERNAL_FLASH_SERVER_DOMAIN_LENGTH - 2)
      {
        server_addr->addr_type = DOMAIN_NAME;
        memcpy(server_addr->domain, buf + 2, buf[1]);

        // null terminate string
        char *p = server_addr->domain + buf[1];
        *p = 0;
        break;
      }
      // else fall through to default

    default:
      server_addr->addr_type = INVALID_INTERNET_ADDRESS;
  }
}

// keyBuffer length must be at least EXTERNAL_FLASH_SERVER_PUBLIC_KEY_LENGTH
void FLASH_Read_ServerPublicKey(uint8_t *keyBuffer)
{
    sFLASH_ReadBuffer(keyBuffer,
            EXTERNAL_FLASH_SERVER_PUBLIC_KEY_ADDRESS,
            EXTERNAL_FLASH_SERVER_PUBLIC_KEY_LENGTH);
}

// keyBuffer length must be at least EXTERNAL_FLASH_CORE_PRIVATE_KEY_LENGTH
void FLASH_Read_CorePrivateKey(uint8_t *keyBuffer)
{
    sFLASH_ReadBuffer(keyBuffer,
            EXTERNAL_FLASH_CORE_PRIVATE_KEY_ADDRESS,
            EXTERNAL_FLASH_CORE_PRIVATE_KEY_LENGTH);

}

void FACTORY_Flash_Reset(void)
{
  // Restore the Factory programmed application firmware from External Flash
  FLASH_Restore(EXTERNAL_FLASH_FAC_ADDRESS);

  Factory_Reset_SysFlag = 0xFFFFFFFF;

  Finish_Update();
}

void BACKUP_Flash_Reset(void)
{
    //Restore the Backup programmed application firmware from External Flash
    FLASH_Restore(EXTERNAL_FLASH_BKP_ADDRESS);

    Finish_Update();
}

void OTA_Flash_Reset(void)
{
    //First take backup of the current application firmware to External Flash
    FLASH_Backup(EXTERNAL_FLASH_BKP_ADDRESS);

    FLASH_OTA_Update_SysFlag = 0x5555;
    Save_SystemFlags();
    RTC_WriteBackupRegister(RTC_BKP_DR10, 0x5555);

    //Restore the OTA programmed application firmware from External Flash
    FLASH_Restore(EXTERNAL_FLASH_OTA_ADDRESS);

    OTA_FLASHED_Status_SysFlag = 0x0001;

    Finish_Update();
}

bool OTA_Flashed_GetStatus(void)
{
    if(OTA_FLASHED_Status_SysFlag == 0x0001)
        return true;
    else
        return false;
}

void OTA_Flashed_ResetStatus(void)
{
    OTA_FLASHED_Status_SysFlag = 0x0000;
    Save_SystemFlags();
}

/*******************************************************************************
* Function Name  : Finish_Update.
* Description    : Reset the device.
* Input          : None.
* Return         : None.
*******************************************************************************/
void Finish_Update(void)
{
    FLASH_OTA_Update_SysFlag = 0x5000;
    Save_SystemFlags();

    RTC_WriteBackupRegister(RTC_BKP_DR10, 0x5000);

    NVIC_SystemReset();
}

void Outlet_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Enable the GPIO Clock */
    RCC_AHB1PeriphClockCmd(OUTLET_GPIO_CLK[OUTLET0]   |
                            OUTLET_GPIO_CLK[OUTLET1]  |
                            OUTLET_GPIO_CLK[OUTLET2]  |
                            OUTLET_GPIO_CLK[OUTLET3]  |
                            OUTLET_GPIO_CLK[OUTLET4]  |
                            OUTLET_GPIO_CLK[OUTLET5], ENABLE);

    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Low_Speed;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

    int i;
    for(i = 0; i < OUTLETn; i++)
    {
        GPIO_InitStructure.GPIO_Pin = OUTLET_GPIO_PIN[i];
        GPIO_Init(OUTLET_GPIO_PORT[i], &GPIO_InitStructure);
        GPIO_ResetBits(OUTLET_GPIO_PORT[i], OUTLET_GPIO_PIN[i]);
    }
}

void Outlet_GPIO_On(int outlet)
{
    GPIO_SetBits(OUTLET_GPIO_PORT[outlet], OUTLET_GPIO_PIN[outlet]);
}

void Outlet_GPIO_Off(int outlet)
{
    GPIO_ResetBits(OUTLET_GPIO_PORT[outlet], OUTLET_GPIO_PIN[outlet]);
}

void Set_Outlet_Register(uint32_t outletMask)
{
    RTC_WriteBackupRegister(RTC_BKP_DR11, outletMask);
}

uint32_t Get_Outlet_Register(void)
{
    return RTC_ReadBackupRegister(RTC_BKP_DR11);
}

void ADC_Init(void)
{
    /* To program the configuration register, bring the CS and WR low and apply
     * the required configuration data on CR3–CR0 of the bus and then raise WR
     * once to save changes.
     */
    GPIO_InitTypeDef  GPIO_InitStructure;

    /* Configure CR pins as outputs first */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;

    int i;
    for(i = 0; i < 4; i++)
    {
        RCC_AHB1PeriphClockCmd(ADC_CR_CLK[i], ENABLE);
        GPIO_InitStructure.GPIO_Pin = ADC_CR_PIN[i];
        GPIO_Init(ADC_CR_PORT[i], &GPIO_InitStructure);
    }

    /* Configure CONVST, WR*, RD*, and CS* outputs */
    RCC_AHB1PeriphClockCmd(ADC_CONVST_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = ADC_CONVST_PIN;
    GPIO_Init(ADC_CONVST_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(ADC_CONVST_PORT, ADC_CONVST_PIN);

    RCC_AHB1PeriphClockCmd(ADC_WR_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = ADC_WR_PIN;
    GPIO_Init(ADC_WR_PORT, &GPIO_InitStructure);
    GPIO_ResetBits(ADC_WR_PORT, ADC_WR_PIN);

    RCC_AHB1PeriphClockCmd(ADC_RD_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = ADC_RD_PIN;
    GPIO_Init(ADC_RD_PORT, &GPIO_InitStructure);
    GPIO_SetBits(ADC_RD_PORT, ADC_RD_PIN);

    RCC_AHB1PeriphClockCmd(ADC_CS_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = ADC_CS_PIN;
    GPIO_Init(ADC_CS_PORT, &GPIO_InitStructure);
    GPIO_SetBits(ADC_CS_PORT, ADC_CS_PIN);


    /* To program the configuration register, bring the CS and WR low and apply
     * the required configuration data on CR3–CR0 of the bus and then raise WR
     * once to save changes.
     */
    GPIO_ResetBits(ADC_CS_PORT, ADC_CS_PIN);
    GPIO_ResetBits(ADC_WR_PORT, ADC_WR_PIN);

    /* CR3 selects the internal or external reference. POR default = 0
     * 0 = internal reference, 1 = external reference 
     */
    GPIO_ResetBits(ADC_DB3_PORT, ADC_DB3_PIN);

    /* CR2 selects the output data format. POR default = 0.
     * 0 = offset binary. 1 = two’s complement.
     */
    GPIO_SetBits(ADC_DB2_PORT, ADC_DB2_PIN);

    /* CR1 (Reserved) CR1 must be set to 0. */
    GPIO_ResetBits(ADC_DB1_PORT, ADC_DB1_PIN);

    /* CR0 (CONVST Mode) CR0 selects the acquisition mode. POR default = 0.
     * 0 = CONVST controls the acquisition and conversion.
     * Drive CONVST low to start acquisition. The rising edge of CONVST begins 
     * the conversion. 
     * 1 = acquisition mode starts as soon as the previous conversion is 
     * complete. The rising edge of CONVST begins the conversion.
     */
    GPIO_SetBits(ADC_DB0_PORT, ADC_DB0_PIN);

    /* Raise WR to save changes of CR */
    GPIO_SetBits(ADC_WR_PORT, ADC_WR_PIN);
    GPIO_SetBits(ADC_CS_PORT, ADC_CS_PIN);

    /* Configure DB0-DB15 as inputs */
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

    int j;
    for(j = 0; j < 16; j++)
    {
        RCC_AHB1PeriphClockCmd(ADC_DI_CLK[j], ENABLE);
        GPIO_InitStructure.GPIO_Pin = ADC_DI_PIN[j];
        GPIO_Init(ADC_DI_PORT[j], &GPIO_InitStructure);
    }

    /* Configure EOC* as input */
    RCC_AHB1PeriphClockCmd(ADC_EOC_CLK, ENABLE);
    GPIO_InitStructure.GPIO_Pin = ADC_EOC_PIN;
    GPIO_Init(ADC_EOC_PORT, &GPIO_InitStructure);

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

    /* Configure EOC* External Interrupt (EXTI) */
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = ADC_EOC_EXTI_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = ADC_EOC_IRQ_PRIORITY;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    EXTI_InitTypeDef EXTI_InitStructure;

    /* Connect EXTI Line to appropriate GPIO Pin */
    SYSCFG_EXTILineConfig(ADC_EOC_EXTI_PORT_SOURCE, ADC_EOC_EXTI_PIN_SOURCE);

    /* Clear the EXTI line pending flag */  
    EXTI_ClearFlag(ADC_EOC_EXTI_LINE);

    /* Configure Button EXTI line */
    EXTI_InitStructure.EXTI_Line = ADC_EOC_EXTI_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = ADC_EOC_EXTI_TRIGGER;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

}

void ADC_StartConversion(void)
{
    GPIO_SetBits(ADC_CONVST_PORT, ADC_CONVST_PIN);
}

void ADC_Timer_Configure(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;

    /* Enable the TIM3 gloabal Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = TIM3_CC_IRQ_PRIORITY;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure); 

    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
    TIM_OCInitTypeDef TIM_OCInitStructure;

    /* Enable TIM3 clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

    /* TIM5_Prescaler: 42 */
    uint16_t TIM3_Prescaler = 42;
    uint16_t TIM3_Autoreload = (1000000 / 20000) - 1;

    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);

    /* Time Base Configuration */
    TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
    TIM_TimeBaseStructure.TIM_Period = TIM3_Autoreload;
    TIM_TimeBaseStructure.TIM_Prescaler = TIM3_Prescaler;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0x0000;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

    /* Output Compare Timing Mode configuration: Channel 4 */
    TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
    TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCInitStructure.TIM_Pulse = 0x0000;
    TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

    TIM_OC4Init(TIM3, &TIM_OCInitStructure);
    TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Disable);

    TIM_ARRPreloadConfig(TIM3, ENABLE);

    /* TIM5 enable counter */
    TIM_Cmd(TIM3, ENABLE);

    /* Enable TIM1 CC4 Interrupt */
    TIM_ITConfig(TIM3, TIM_IT_CC4, ENABLE);
}

int16_t ADC_Read(void)
{
    uint16_t result;

    result = GPIO_ReadInputDataBit(ADC_DB0_PORT, ADC_DB0_PIN);
    result |= (GPIO_ReadInputDataBit(ADC_DB1_PORT, ADC_DB1_PIN) << 1);
    result |= (GPIO_ReadInputDataBit(ADC_DB2_PORT, ADC_DB2_PIN) << 2);
    result |= (GPIO_ReadInputDataBit(ADC_DB3_PORT, ADC_DB3_PIN) << 3);
    result |= (GPIO_ReadInputDataBit(ADC_DB4_PORT, ADC_DB4_PIN) << 4);
    result |= (GPIO_ReadInputDataBit(ADC_DB5_PORT, ADC_DB5_PIN) << 5);
    result |= (GPIO_ReadInputDataBit(ADC_DB6_PORT, ADC_DB6_PIN) << 6);
    result |= (GPIO_ReadInputDataBit(ADC_DB7_PORT, ADC_DB7_PIN) << 7);
    result |= (GPIO_ReadInputDataBit(ADC_DB8_PORT, ADC_DB8_PIN) << 8);
    result |= (GPIO_ReadInputDataBit(ADC_DB9_PORT, ADC_DB9_PIN) << 9);
    result |= (GPIO_ReadInputDataBit(ADC_DB10_PORT, ADC_DB10_PIN) << 10);
    result |= (GPIO_ReadInputDataBit(ADC_DB11_PORT, ADC_DB11_PIN) << 11);
    result |= (GPIO_ReadInputDataBit(ADC_DB12_PORT, ADC_DB12_PIN) << 12);
    result |= (GPIO_ReadInputDataBit(ADC_DB13_PORT, ADC_DB13_PIN) << 13);
    result |= (GPIO_ReadInputDataBit(ADC_DB14_PORT, ADC_DB14_PIN) << 14);
    result |= (GPIO_ReadInputDataBit(ADC_DB15_PORT, ADC_DB15_PIN) << 15);

    return result;

}

/**
  * @brief  Computes the 32-bit CRC of a given buffer of byte data.
  * @param  pBuffer: pointer to the buffer containing the data to be computed
  * @param  BufferSize: Size of the buffer to be computed
  * @retval 32-bit CRC
  */
uint32_t Compute_CRC32(uint8_t *pBuffer, uint32_t bufferSize)
{
    uint32_t i, j;
    uint32_t Data;

    CRC_ResetDR();

    i = bufferSize >> 2;

    while (i--)
    {
        Data = *((uint32_t *)pBuffer);
        pBuffer += 4;

        Data = __RBIT(Data);//reverse the bit order of input Data
        CRC->DR = Data;
    }

    Data = CRC->DR;
    Data = __RBIT(Data);//reverse the bit order of output Data

    i = bufferSize & 3;

    while (i--)
    {
        Data ^= (uint32_t)*pBuffer++;

        for (j = 0 ; j < 8 ; j++)
        {
            if (Data & 1)
                Data = (Data >> 1) ^ 0xEDB88320;
            else
                Data >>= 1;
        }
    }

    Data ^= 0xFFFFFFFF;

    return Data;
}

void Get_Unique_Device_ID(uint8_t *Device_ID)
{
  uint32_t Device_IDx;

  Device_IDx = *(uint32_t*)ID1;
  *Device_ID++ = (uint8_t)(Device_IDx & 0xFF);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF00) >> 8);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF0000) >> 16);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF000000) >> 24);

  Device_IDx = *(uint32_t*)ID2;
  *Device_ID++ = (uint8_t)(Device_IDx & 0xFF);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF00) >> 8);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF0000) >> 16);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF000000) >> 24);

  Device_IDx = *(uint32_t*)ID3;
  *Device_ID++ = (uint8_t)(Device_IDx & 0xFF);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF00) >> 8);
  *Device_ID++ = (uint8_t)((Device_IDx & 0xFF0000) >> 16);
  *Device_ID = (uint8_t)((Device_IDx & 0xFF000000) >> 24);
}

static volatile system_tick_t system_1ms_tick = 0;

void System1MsTick(void)
{
    system_1ms_tick++;
}

system_tick_t GetSystem1MsTick()
{
    return system_1ms_tick;
}
